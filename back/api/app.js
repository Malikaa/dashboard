const express = require("express");
const app = express();
const api = require('./index');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const connection = mongoose.connection;

// app.set('port', process.env.port || 3000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use('/api/', api);

app.listen(3000, () => {
    console.log('connecter au serveur')
})

app.use((req, res) => {
    const err = new Error('404 - Not found !!!');
    err.status = 500;
    res.json({ msg: '404 - Not found !!!', err: err });
})

mongoose.connect('mongodb://localhost:27017/dashboard', { useNewUrlParser: true });
connection.on('error', (err) => {
    console.error(`connection to MongoDB error:${err.message}`)
});
//ouverture de la BDD
connection.once('open', () => {
    console.log(`Connected to MongoDB`);

})






