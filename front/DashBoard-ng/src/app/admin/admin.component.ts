import { Component, OnInit } from '@angular/core';
import { BlogpostService } from '../blogpost.service';
import { Blogpost } from '../models/blogpost';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  blogposts$: Observable<Blogpost[]>
  allBlogposts: Blogpost[];
  constructor(private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.blogposts$=this.blogpostService.getBlogposts();
    this.blogpostService
      .getBlogposts()
      .subscribe(data => this.refresh(data))

    this.blogpostService
      .handaleBlogpostCreated()
      .subscribe(data => {
        this.refresh(data);
      })
  }
  refresh(data) {
    console.log('data', data)
    this.blogpostService.getBlogposts().subscribe(data => {
      this.allBlogposts = data
    })
  }
}


