import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Blogpost } from './models/blogpost';
@Injectable({
    providedIn: 'root'

})
export class BlogpostService {
    baseUrl = 'http://localhost:3000/api/blog-posts';

    private blogpostCreated = new Subject<string>();
    constructor(private httpClient: HttpClient) { }
   
    createBlogpost(blogpost: Blogpost) {
        return this.httpClient.post<Blogpost>(this.baseUrl, blogpost);
    }
    dispatchBlogpostCreated(id: string) {
        this.blogpostCreated.next(id);
    }
    handaleBlogpostCreated() {
        return this.blogpostCreated.asObservable();
    }

    getBlogposts(): Observable<Blogpost[]> {
        return this.httpClient.get<Blogpost[]>(`${this.baseUrl}/`)
    }
    getBlogpostsByid(id): Observable<Blogpost> {
        return this.httpClient.get<Blogpost>(`${this.baseUrl}/${id}`)
    }

}
