import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormGroupDirective } from '@angular/forms';
import { Blogpost } from '../models/blogpost';
import { BlogpostService } from '../blogpost.service';

@Component({
  selector: 'app-create-blog',
  templateUrl: './create-blog.component.html',
  styleUrls: ['./create-blog.component.css']
})
export class CreateBlogComponent implements OnInit {
  creationForm: FormGroup;

  constructor(private fb: FormBuilder, private blogpostService: BlogpostService) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.creationForm = this.fb.group({
      title: '',
      subTitle: '',
      content: ''
    });
  }
  createBlogpost(formDerctive: FormGroupDirective) {
    if (this.creationForm.valid) {
      console.log(this.creationForm.value)
      this.blogpostService
        .createBlogpost(this.creationForm.value)
        .subscribe(data => this.handleSucess(data, formDerctive), error => this.handleError(error))
    }
  }
  handleSucess(data, formDerctive) {
    console.log('ok blog post created', data)
    this.creationForm.reset();
    formDerctive.resetForm();
    this.blogpostService.dispatchBlogpostCreated(data._id)
  }
  handleError(error) {
    console.log('blog post not created', error)
  }
}
